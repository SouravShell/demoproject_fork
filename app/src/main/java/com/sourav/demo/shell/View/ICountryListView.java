package com.sourav.demo.shell.View;

import com.sourav.demo.shell.model.pojo.countrylist.CountryListResponse;

import java.util.List;

/**
 * Created by Sourav on 09/02/19.
 */
public interface ICountryListView {

    void NavigateToDetailPage(String Countryname) ;

    void showProgressBar();

    void dismissProgressBar(List<CountryListResponse> countryListResponses);
}
