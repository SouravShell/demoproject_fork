package com.sourav.demo.shell.model.pojo.countrylist;

import android.util.Log;

import com.sourav.demo.shell.Activity.CountryMainActivity;
import com.sourav.demo.shell.net.Api;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.BaseObservable;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sourav on 09/02/19.
 */
public class CountryList extends BaseObservable {

    private List<CountryListResponse> countryListResponses = new ArrayList<>();
    private MutableLiveData<List<CountryListResponse>> countries = new MutableLiveData<>();
    public MutableLiveData<List<CountryListResponse>> getCountries() {
        return countries;
    }


    public void fetchList(CountryMainActivity countryMainActivity) {
        Callback<List<CountryListResponse>> callback1 = new Callback<List<CountryListResponse>>() {
            @Override
            public void onResponse(Call<List<CountryListResponse>> call, Response<List<CountryListResponse>> response) {
                setCountryListResponses(response.body());
                countries.setValue(countryListResponses);
            }

            @Override
            public void onFailure(Call<List<CountryListResponse>> call, Throwable t) {
                Log.e("Test", t.getMessage(), t);
            }

        };

        new Api(countryMainActivity).getApi().getAllCountriesData().enqueue(callback1);

    }

    public void setCountryListResponses(List<CountryListResponse> countryListResponses) {
        this.countryListResponses = countryListResponses;
    }

}
