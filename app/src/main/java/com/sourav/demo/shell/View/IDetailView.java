package com.sourav.demo.shell.View;

/**
 * Created by Sourav on 09/02/19.
 */
public interface IDetailView {

    void displayCountryName(String name);

    void displayFlag(String flag);

    void displayCapital(String s);

    void displayCallingCode(String substring);

    void displayRegion(String region);

    void displayTimeZones(String timeZones);

    void displayLanguages(String languages);
}
