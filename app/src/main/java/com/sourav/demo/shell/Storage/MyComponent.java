package com.sourav.demo.shell.Storage;

import com.sourav.demo.shell.Activity.CountryDetailActivity;
import com.sourav.demo.shell.Activity.CountryMainActivity;

import dagger.Component;
/**
 * Created by Sourav on 08/02/19.
 */
@Component(modules = {SharedPreferencesModule.class,NetworkModule.class})
@MyApplicationScope
public interface MyComponent {
    void inject(CountryMainActivity mainActivity);

    void inject(CountryDetailActivity countryDetailActivity);
}
