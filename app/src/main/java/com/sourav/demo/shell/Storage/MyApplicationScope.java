package com.sourav.demo.shell.Storage;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;
/**
 * Created by Sourav on 08/02/19.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface MyApplicationScope {
}
