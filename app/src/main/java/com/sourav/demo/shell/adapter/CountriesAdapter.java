package com.sourav.demo.shell.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.sourav.demo.shell.BR;
import com.sourav.demo.shell.model.pojo.countrylist.CountryListResponse;
import com.sourav.demo.shell.viewmodel.CountriesViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;


/**
 * Created by Sourav on 10/02/19.
 */
public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.GenericViewHolder> implements Filterable {

    private int layoutId;
    private List<CountryListResponse> countryLists;
    private List<CountryListResponse> countryListsFilters;
    private CountriesViewModel viewModel;

    public CountriesAdapter(@LayoutRes int layoutId, CountriesViewModel viewModel) {
        this.layoutId = layoutId;
        this.viewModel = viewModel;
    }

    private int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @Override
    public int getItemCount() {
        return countryListsFilters == null ? 0 : countryListsFilters.size();
    }

    public GenericViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);

        return new GenericViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GenericViewHolder holder, int position) {
        holder.bind(viewModel, position);
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    public void setCountryLists(List<CountryListResponse> countries) {
        this.countryLists = countries;
        this.countryListsFilters=countries;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    countryListsFilters.clear();
                } else {
                    List<CountryListResponse> filteredList = new ArrayList<>();
                    for (CountryListResponse country : countryLists) {

                        // name match condition. this might differ depending on  requirement
                        // here we are looking for country name match
                        if (country.name.toLowerCase().contains(charString.toLowerCase()) || country.name.contains(charSequence)) {
                            filteredList.add(country);
                        }
                    }

                    countryListsFilters = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = countryListsFilters;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                countryListsFilters = (List<CountryListResponse>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    class GenericViewHolder extends RecyclerView.ViewHolder {
        final ViewDataBinding binding;

        GenericViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(CountriesViewModel viewModel, Integer position) {
            binding.setVariable(BR.viewModel, viewModel);
            binding.setVariable(BR.position, position);
            binding.executePendingBindings();
        }

    }


}
