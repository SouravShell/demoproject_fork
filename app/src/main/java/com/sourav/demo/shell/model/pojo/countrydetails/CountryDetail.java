package com.sourav.demo.shell.model.pojo.countrydetails;

import android.util.Log;

import com.sourav.demo.shell.Activity.CountryDetailActivity;
import com.sourav.demo.shell.net.Api;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.BaseObservable;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sourav on 10/02/19.
 */
public class CountryDetail extends BaseObservable {

    private List<CountryDetailResponse> countryDetailResponses = new ArrayList<>();

    private MutableLiveData<List<CountryDetailResponse>> countryDetailResponsesDetail = new MutableLiveData<List<CountryDetailResponse>>();
    public MutableLiveData<List<CountryDetailResponse>> getCountryDetails() {
        return countryDetailResponsesDetail;
    }

    public CountryDetail() {
        countryDetailResponsesDetail.setValue(countryDetailResponses);
    }

    public void fetchCountryDetail(String countryName, CountryDetailActivity countryDetailActivity) {
        Callback<List<CountryDetailResponse>> callback = new Callback<List<CountryDetailResponse>>() {
            @Override
            public void onResponse(Call<List<CountryDetailResponse>> call, Response<List<CountryDetailResponse>> response) {
                countryDetailResponses=response.body();
                countryDetailResponsesDetail.setValue(countryDetailResponses);
            }

            @Override
            public void onFailure(Call<List<CountryDetailResponse>> call, Throwable t) {
                Log.e("Fail====", t.getMessage(), t);
            }


        };



        new Api(countryDetailActivity).getApi().getCountryDetail(countryName).enqueue(callback);
    }
}
