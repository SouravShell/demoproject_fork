package com.sourav.demo.shell.model.pojo.countrylist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by Sourav on 09/02/19.
 */
public class Language {

    @SerializedName("iso639_1")
    @Expose
    public String iso6391;
    @SerializedName("iso639_2")
    @Expose
    public String iso6392;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("nativeName")
    @Expose
    public String nativeName;
}

