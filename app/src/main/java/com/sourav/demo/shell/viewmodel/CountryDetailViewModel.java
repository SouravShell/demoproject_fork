package com.sourav.demo.shell.viewmodel;

import android.view.View;

import com.sourav.demo.shell.Activity.CountryDetailActivity;
import com.sourav.demo.shell.View.IDetailView;
import com.sourav.demo.shell.model.pojo.countrydetails.CountryDetail;
import com.sourav.demo.shell.model.pojo.countrydetails.CountryDetailResponse;

import java.util.List;

import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 * Created by Sourav on 10/02/19.
 */
public class CountryDetailViewModel extends ViewModel {

    private CountryDetail countryDetail;
    public ObservableInt loading;
    public ObservableInt showEmpty;
    private List<CountryDetailResponse> countryDetailResponses;
    IDetailView detailView;

    public void init(IDetailView countryDetailActivity) {
        countryDetail = new CountryDetail();

        this.detailView=countryDetailActivity;
        loading = new ObservableInt(View.GONE);
        showEmpty = new ObservableInt(View.GONE);
    }

    public MutableLiveData<List<CountryDetailResponse>> getCountryDetail() {
        return countryDetail.getCountryDetails();
    }

    public void setCountryDetail(List<CountryDetailResponse> countryDetailResponses) {
        this.countryDetailResponses=countryDetailResponses;
    }


    public void fetchDetails(String name) {
        countryDetail.fetchCountryDetail(name,(CountryDetailActivity) detailView);
    }

    public void setCountryName(){
        detailView.displayCountryName("COUNTRYNAME-"+countryDetailResponses.get(0).getName());
    }

    public void setFlag() {
        detailView.displayFlag(countryDetailResponses.get(0).getFlag());
    }

    public void setCapital() {
        detailView.displayCapital("CAPITAL-"+countryDetailResponses.get(0).getCapital());
    }

    public void setCallingCode() {

        StringBuffer callingcode= new StringBuffer();
        countryDetailResponses.get(0).callingCodes.stream().forEach(s ->{
                    callingcode.append(s+",");
                    detailView.displayCallingCode(callingcode.toString().substring(0,callingcode.toString().length()-1));
                }

        );
    }

    public void setRegion() {
        detailView.displayRegion("REGION-"+countryDetailResponses.get(0).getRegion());

    }

    public void setTimeZones() {
        StringBuffer timeZones= new StringBuffer();
        countryDetailResponses.get(0).getTimezones().stream().forEach(s ->{
                    timeZones.append(s+",");
        detailView.displayTimeZones("TIMEZONE-"+timeZones.toString().substring(0,timeZones.toString().length()-1));
        }

        );
    }

    public void setLanguages() {
        StringBuffer languages= new StringBuffer();
        countryDetailResponses.get(0).languages.stream().forEach(s ->{
                    languages.append("Language name -"+s.getName()+"NativeName-"+s.getNativeName()+",ISO6391-"+s.getIso6391()+",ISO6392"+s.getIso6392()+",");
            detailView.displayLanguages("LANGUAGE DETAIL-"+languages.toString().substring(0,languages.toString().length()-1));

                }

        );
    }
}
