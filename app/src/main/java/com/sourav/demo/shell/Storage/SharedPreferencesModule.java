package com.sourav.demo.shell.Storage;

import android.content.Context;
import android.content.SharedPreferences;

import dagger.Module;
import dagger.Provides;
/**
 * Created by Sourav on 08/02/19.
 */
@Module
public class SharedPreferencesModule {

    private Context context;

    public SharedPreferencesModule(Context context) {
        this.context = context;
    }


    @MyApplicationScope
    @Provides
    SharedPreferences provideSharedPreferences() {
        return context.getSharedPreferences("PrefName",Context.MODE_PRIVATE);
    }
}
