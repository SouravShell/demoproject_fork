package com.sourav.demo.shell.Storage;

import android.content.Context;

import com.sourav.demo.shell.net.Utilities;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Sourav on 08/02/19.
 */
@Module
public class NetworkModule {

    private Context context;

    public NetworkModule(Context context) {
        this.context = context;
    }

    @MyApplicationScope
    @Provides
    Utilities provideApiManager() {
        return new Utilities(context);
    }

}
