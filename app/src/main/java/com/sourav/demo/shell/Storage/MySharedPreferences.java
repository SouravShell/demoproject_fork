package com.sourav.demo.shell.Storage;

import android.content.SharedPreferences;

import javax.inject.Inject;
/**
 * Created by Sourav on 08/02/19.
 */
public class MySharedPreferences {

    private SharedPreferences mSharedPreferences;
    public static final String MapList="MAPLIST";

    @Inject
    public MySharedPreferences(SharedPreferences mSharedPreferences) {
        this.mSharedPreferences = mSharedPreferences;
    }

    public void putData(String key, String data) {
        mSharedPreferences.edit().putString(key,data).apply();
    }

    public String getData(String key) {
        return mSharedPreferences.getString(key,"");
    }

    public boolean deleteByKey(String key){
       return mSharedPreferences.edit().remove(key).commit();
    }

}
