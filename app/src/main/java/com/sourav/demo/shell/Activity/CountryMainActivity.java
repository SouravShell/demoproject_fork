package com.sourav.demo.shell.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.sourav.demo.shell.MainApplication;
import com.sourav.demo.shell.R;
import com.sourav.demo.shell.Storage.MySharedPreferences;
import com.sourav.demo.shell.View.ICountryListView;
import com.sourav.demo.shell.databinding.ActivityCountryListBinding;
import com.sourav.demo.shell.model.pojo.countrylist.CountryListResponse;
import com.sourav.demo.shell.net.Utilities;
import com.sourav.demo.shell.viewmodel.CountriesViewModel;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

/**
 * Created by Sourav on 09/02/19.
 */
public class CountryMainActivity extends BaseActivity implements ICountryListView {

    private CountriesViewModel viewModel;
    @Inject
    MySharedPreferences mySharedPreferences;

    @Inject
    Utilities utilities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ((MainApplication)getApplicationContext()).getMyComponent().inject(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_country_list);

        setupBindings(savedInstanceState);

    }

    private void setupBindings(Bundle savedInstanceState) {

        ActivityCountryListBinding activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_country_list);

        viewModel = ViewModelProviders.of(this).get(CountriesViewModel.class);
        if (savedInstanceState == null) {
            viewModel.init(this);
        }
        activityBinding.setModel(viewModel);
        setupListUpdate();
        activityBinding.search.setActivated(true);
        activityBinding.search.onActionViewExpanded();
        activityBinding.search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                viewModel.setFilter(newText);
                return false;
            }
        });

    }

    private void setupListUpdate() {

        viewModel.loading.set(View.VISIBLE);

        viewModel.fetchList();
        viewModel.getCountries().observe(this, countryListResponses -> {
            viewModel.loading.set(View.GONE);
            viewModel.getCountryList(countryListResponses);
        });
        setupListClick();
    }

   private void setupListClick() {
        viewModel.getSelected().observe(this, countryList -> {
            if (countryList != null) {
                viewModel.NavigateToDetailPage(countryList.name);

            }
        });
    }

    @Override
    public void NavigateToDetailPage(String countryName) {
        Intent intent = new Intent(CountryMainActivity.this, CountryDetailActivity.class);
        intent.putExtra(CountryDetailActivity.KEY_COUNTRY_NAME,countryName);
        startActivity(intent);
    }

    @Override
    public void showProgressBar() {
        viewModel.showEmpty.set(View.VISIBLE);
    }

    @Override
    public void dismissProgressBar(List<CountryListResponse> countryListResponses) {
        viewModel.showEmpty.set(View.GONE);
        viewModel.setCountryInAdapter(countryListResponses);

    }
}
