package com.sourav.demo.shell.Storage;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sourav on 08/02/19.
 */
@Module
public class ContextModule {
    private Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @MyApplicationScope
    Context provideContext() {
        return context;
    }
}
