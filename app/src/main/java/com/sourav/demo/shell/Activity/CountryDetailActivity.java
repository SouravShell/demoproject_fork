package com.sourav.demo.shell.Activity;

import android.os.Bundle;
import android.view.View;

import com.guardanis.imageloader.ImageRequest;
import com.sourav.demo.shell.MainApplication;
import com.sourav.demo.shell.R;
import com.sourav.demo.shell.View.IDetailView;
import com.sourav.demo.shell.databinding.ViewCountryDetailsBinding;
import com.sourav.demo.shell.viewmodel.CountryDetailViewModel;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;


/**
 * Created by Sourav on 09/02/19.
 * Detail Activity Page
 */

public class CountryDetailActivity extends BaseActivity implements IDetailView {

    CountryDetailViewModel countryDetailViewModel;
    ViewCountryDetailsBinding activityBinding;
    public static String KEY_COUNTRY_NAME="COUNTRY_NAME";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainApplication)getApplicationContext()).getMyComponent().inject(this);
        setContentView(R.layout.view_country_details);
        setupBindings(savedInstanceState);
    }

    private void setupBindings(Bundle savedInstanceState) {

        activityBinding = DataBindingUtil.setContentView(this, R.layout.view_country_details);
        countryDetailViewModel = ViewModelProviders.of(this).get(CountryDetailViewModel.class);
        if (savedInstanceState == null) {
            countryDetailViewModel.init(this);
        }
        activityBinding.setDetailViewModel(countryDetailViewModel);
        setupDetail();

    }


    private void setupDetail() {
        countryDetailViewModel.loading.set(View.VISIBLE);

        String name = getIntent().getExtras().getString(KEY_COUNTRY_NAME);

        countryDetailViewModel.fetchDetails(name.trim());
        countryDetailViewModel.getCountryDetail().observe(this, countryDetailResponses -> {
            countryDetailViewModel.loading.set(View.GONE);
            if (countryDetailResponses.size()== 0) {
                countryDetailViewModel.showEmpty.set(View.VISIBLE);

            } else {

                countryDetailViewModel.showEmpty.set(View.GONE);
                countryDetailViewModel.setCountryDetail(countryDetailResponses);
                countryDetailViewModel.setCountryName();
                countryDetailViewModel.setFlag();
                countryDetailViewModel.setCapital();
                countryDetailViewModel.setCallingCode();
                countryDetailViewModel.setRegion();
                countryDetailViewModel.setTimeZones();
                countryDetailViewModel.setLanguages();

            }
        });

    }



    @Override
    public void displayCountryName(String name) {
        activityBinding.txtName.setText("COUNTRYNAME-"+name);
    }

    @Override
    public void displayFlag(String flagUrl) {
        ImageRequest.create(activityBinding.imageview)
                .setTargetUrl(flagUrl)
                .setFadeTransition(150)/**/
                .setTranslateTransition(-1f, -1f, 350)
                .execute();
    }

    @Override
    public void displayCapital(String capital) {
        activityBinding.capital.setText(capital);
    }

    @Override
    public void displayCallingCode(String callingCode) {
        activityBinding.callingcode.setText(callingCode);

    }

    @Override
    public void displayRegion(String region) {
        activityBinding.region.setText(region);
    }

    @Override
    public void displayTimeZones(String timeZones) {
        activityBinding.timezone.setText(timeZones);
    }

    @Override
    public void displayLanguages(String languages) {
        activityBinding.languages.setText(languages);
    }
}
