package com.sourav.demo.shell.viewmodel;

import android.view.View;

import com.sourav.demo.shell.Activity.CountryMainActivity;
import com.sourav.demo.shell.R;
import com.sourav.demo.shell.View.ICountryListView;
import com.sourav.demo.shell.adapter.CountriesAdapter;
import com.sourav.demo.shell.model.pojo.countrylist.CountryList;
import com.sourav.demo.shell.model.pojo.countrylist.CountryListResponse;

import java.util.List;

import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 * Created by Sourav on 10/02/19.
 */
public class CountriesViewModel extends ViewModel {

    private CountryList countryList;
    private CountriesAdapter adapter;
    public MutableLiveData<CountryListResponse> selected;
    public ObservableInt loading;
    public ObservableInt showEmpty;
    private List<CountryListResponse> countryListResponses;
    private ICountryListView iCountryListView;

    public void init(ICountryListView iCountryListView) {

        this.iCountryListView=iCountryListView;
        countryList = new CountryList();
        selected = new MutableLiveData<>();
        adapter = new CountriesAdapter(R.layout.view_country_item, this);
        loading = new ObservableInt(View.GONE);
        showEmpty = new ObservableInt(View.GONE);
    }

    public void fetchList() {
        countryList.fetchList((CountryMainActivity)iCountryListView);
    }

    public MutableLiveData<List<CountryListResponse>> getCountries() {
        return countryList.getCountries();
    }

    public CountriesAdapter getAdapter() {
        return adapter;
    }

    public void setCountryInAdapter(List<CountryListResponse> countryListResponses) {
        this.countryListResponses=countryListResponses;
    }

    public MutableLiveData<CountryListResponse> getSelected() {
        return selected;
    }

    public void onItemClick(Integer index) {
        CountryListResponse db = getCountryAt(index);
        selected.setValue(db);
    }

    public CountryListResponse getCountryAt(Integer index) {
        if (countryList.getCountries().getValue() != null &&
                index != null &&
                countryList.getCountries().getValue().size() > index) {
            return countryList.getCountries().getValue().get(index);
        }
        return null;
    }

    public void setFilter(String newText) {

        if(newText.length()>=1) {
          this.adapter.setCountryLists(countryListResponses);
          this.adapter.notifyDataSetChanged();
      }else
            adapter.getFilter().filter(" ");
            adapter.getFilter().filter(newText);
    }

    public void NavigateToDetailPage(String name) {
        iCountryListView.NavigateToDetailPage(name);
    }

    public void getCountryList(List<CountryListResponse> countryListResponses) {
        if(countryListResponses.size()==0)
            iCountryListView.showProgressBar();
        else
            iCountryListView.dismissProgressBar(countryListResponses);

    }
}
