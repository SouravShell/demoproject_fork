package com.sourav.demo.shell.model.pojo.countrylist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
/**
 * Created by Sourav on 09/02/19.
 */
public class RegionalBloc {

    @SerializedName("acronym")
    @Expose
    public String acronym;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("otherAcronyms")
    @Expose
    public List<Object> otherAcronyms = null;
    @SerializedName("otherNames")
    @Expose
    public List<Object> otherNames = null;

}
