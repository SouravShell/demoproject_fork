package com.sourav.demo.shell;

import android.app.Application;

import com.sourav.demo.shell.Storage.DaggerMyComponent;
import com.sourav.demo.shell.Storage.MyComponent;
import com.sourav.demo.shell.Storage.NetworkModule;
import com.sourav.demo.shell.Storage.SharedPreferencesModule;

/**
 * Created by Sourav on 08/02/19.
 */
public class MainApplication extends Application {
    private MyComponent myComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        myComponent = DaggerMyComponent.builder()
                .sharedPreferencesModule(new SharedPreferencesModule(getApplicationContext()))
                .networkModule(new NetworkModule(getApplicationContext()))
                .build();

    }
    public MyComponent getMyComponent() {
        return myComponent;
    }

}
