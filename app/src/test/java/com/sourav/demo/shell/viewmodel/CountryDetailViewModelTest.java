package com.sourav.demo.shell.viewmodel;

import com.sourav.demo.shell.View.IDetailView;
import com.sourav.demo.shell.model.pojo.countrydetails.CountryDetailResponse;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CountryDetailViewModelTest {

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    @Mock
    IDetailView iDetailView;

    @Mock
    private List<CountryDetailResponse> countryDetailResponses;

    @Mock
    CountryDetailResponse countryDetailResponse;

    CountryDetailViewModel viewModel;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        viewModel = new CountryDetailViewModel();
        viewModel.init(iDetailView);
    }

     @Test
    public void test_countryNameIsDisplayCorrectly() {

        //Given
        when(countryDetailResponses.get(0)).thenReturn(countryDetailResponse);
        when(countryDetailResponse.getName()).thenReturn("Afganishtan");

        //When
        viewModel.setCountryDetail(countryDetailResponses);
        viewModel.setCountryName();

        //Then
        verify(iDetailView).displayCountryName("COUNTRYNAME-"+"Afganishtan");

    }

    @Test
    public void test_countryFlagIsDisplayCorrectly() {

        //Given
        when(countryDetailResponses.get(0)).thenReturn(countryDetailResponse);
        when(countryDetailResponse.getFlag()).thenReturn("https://www.flag.com");

        //When
        viewModel.setCountryDetail(countryDetailResponses);
        viewModel.setFlag();

        //Then
        verify(iDetailView).displayFlag("https://www.flag.com");

    }


    @Test
    public void test_countryCapitalIsDisplayCorrectly() {

        //Given
        when(countryDetailResponses.get(0)).thenReturn(countryDetailResponse);
        when(countryDetailResponse.getCapital()).thenReturn("Kabul");

        //When
        viewModel.setCountryDetail(countryDetailResponses);
        viewModel.setCapital();

        //Then
        verify(iDetailView).displayCapital("CAPITAL-"+"Kabul");

    }

    @Test
    public void test_RegionIsDisplayCorrectly() {

        //Given
        when(countryDetailResponses.get(0)).thenReturn(countryDetailResponse);
        when(countryDetailResponse.getRegion()).thenReturn("Asia");

        //When
        viewModel.setCountryDetail(countryDetailResponses);
        viewModel.setRegion();

        //Then
        verify(iDetailView).displayRegion("REGION-"+"Asia");

    }

  /*  @Test
    public void test_TimeZoneIsDisplayCorrectly() {

        //Given
       // CountryDetailResponse tes = countryDetailResponses.get(0);



        when(countryDetailResponses.get(0)).thenReturn(countryDetailResponse);
        when(countryDetailResponses.get(0).getTimezones()).thenReturn(listString);
        when(countryDetailResponses.get(0).getTimezones().get(0)).thenReturn("UTC-04:400");

        //When
        viewModel.setCountryDetail(countryDetailResponses);
        viewModel.setTimeZones();

        //Then
        verify(iDetailView).displayTimeZones("TIMEZONE-"+"UTC-04:400");

    }*/

}