package com.sourav.demo.shell.viewmodel;

import com.sourav.demo.shell.Activity.CountryMainActivity;
import com.sourav.demo.shell.View.ICountryListView;
import com.sourav.demo.shell.View.IDetailView;
import com.sourav.demo.shell.model.pojo.countrylist.CountryListResponse;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CountriesViewModelTest {


    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    @Mock
    private Observer<CountryListResponse> observer;

    @Mock
    ICountryListView iCountryListView;

    CountriesViewModel viewModel;


    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
         viewModel = new CountriesViewModel();
         viewModel.init(iCountryListView);
    }

    @Test
    public void test_clickReturnCorrectObject() {

        List<CountryListResponse> countryListResponses = generateCountryListResponse();

        viewModel.getSelected().observeForever(observer);
        // When
        viewModel.getCountries().setValue(countryListResponses);
        viewModel.onItemClick(0);
        // then
        verify(observer).onChanged(countryListResponses.get(0));
    }

    private List<CountryListResponse> generateCountryListResponse() {
        List<CountryListResponse> CountryListResponse = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            CountryListResponse db = new CountryListResponse();
            CountryListResponse.add(db);
        }
        return CountryListResponse;
    }

    @Test
    public void whenCountryListIsNotEmptyShowProgressBar() {

        //Given
        List<CountryListResponse> CountryListResponse = new ArrayList<>();
        //When
        viewModel.getCountryList(CountryListResponse);
        //then
        verify(iCountryListView).showProgressBar();

    }

   @Test
    public void whenCountryIsEmptyDismissProgressBar() {


        List<CountryListResponse> CountryListResponse = generateCountryListResponse();//iven

        viewModel.getCountryList(CountryListResponse);//When

        verify(iCountryListView).dismissProgressBar(CountryListResponse);//then

        }

    @Test
    public void test_clickNavigateToCorrectDetailPage(){

        viewModel.NavigateToDetailPage("CountryName");//when
        verify(iCountryListView).NavigateToDetailPage("CountryName");//then
    }

}